import random

x = [random.randint(0, 10) for i in range(10)]
y = [random.randint(0, 10) for i in range(10)]

a=[0]*10
print("Координаты точек:")
for i in range(10):
    a[i]=[x[i],y[i]]
print(a)

with open("Koordinaty_tochek.txt", "w") as f:
    for i in range(10):
        f.write('A('+str(i)+') = '+str(a[i])+'\n')
